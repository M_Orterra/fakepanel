import sys
import geopandas as gpd
#append the path of the parent directory
sys.path.append("..")
sys.path.append("../utils")

import utils.classic as classic


if __name__ == "__main__":
	print("Classic Test")
	df =gpd.read_file('input/areasuit1kW.shp')
	output=classic.getClassic(df)
	output.to_file('output/classic.shp')