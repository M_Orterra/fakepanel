import sys

#append the path of the parent directory
sys.path.append("..")
sys.path.append("../utils")

import utils.classic as classic
import utils.selection as selection


if __name__ == "__main__":
    print ("selection test")
    file='../input/torino_processed.shp'
    sections=selection.selectSec(file,0,0)
    suitAreaFile='../input/areasuit1kW.shp'
    areas=selection.selectSuitArea(suitAreaFile,sections)
    panels=selection.selectPanels(areas)
    panels.to_file('output/selectionTest.shp')
