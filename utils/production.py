import numpy as np
import pandas as pd 
from shapely import geometry
import geopandas as gpd
import requests
import time
import json
def powerProduction(G):
    p00 =  45.43
    p10 =  -12.67
    p01 =  -0.08475
    p20 =  2.969
    p11 =  0.01869
    p02 =  4.913e-05
    p30 =  -0.27
    p21 =  -0.001918
    p12 =  -2.492e-06
    p40 =  0.01054;
    p31 =  0.0001177
    p22 =  1.972e-07
    p50 = -0.0001502
    p41 = -2.326e-06
    p32 = -5.239e-09
    v=[0.0*i for i in range(len(G))]
    i=[0.0*i for i in range(len(G))]
    p=[0.0*i for i in range(len(G))]
    for c,g in enumerate(G):
        v[c]=g*0.0006667+ 22.73
        V=g*0.0006667+ 22.73
        p[c]= p00 + p10 * V + p01 * g + p20 * (V**2) + p11 * V * g + p02 * (g**2) + p30 * (V**3)\
                        +p21 * (V**2) * g + p12 * V * (g**2) + p40 * (V**4) + p31 * (V**3) * g\
                        +p22 * (V**2) * g**2 + p50 * (V**5) + p41 * (V**4) * g\
                        +p32 * (V**3) * g**2
        i[c]=p[c]/V
    return v,i,p

y=45.067
x=7.652
r=requests.get(f'https://re.jrc.ec.europa.eu/api/seriescalc?lat={str(y)}&lon=%20{str(x)}&raddatabase=PVGIS-SARAH&01&outputformat=json&select_database_hourly=PVGIS-SARAH&startyear=2013&endyear=2013')
d=r.json()
G=[x['G(i)']*np.cos(np.deg2rad(90-x['H_sun'])) for x in d['outputs']['hourly']]



v,i,p=powerProduction(G)   

def evaluateGroup(geoseries):
    try:
        
        I=[current*len(geoseries)//8 for current in i]
        P=[v[k]*I[k] for k in range(len(I))]
        return P
    except:
        return None

def evaluateAll(panelsFile):
    print('Evaluating all')
    panels=gpd.read_file(panelsFile)
    groups=panels.group.unique()
    powers=[]
    for i,groupID in enumerate(groups):
        print(f'{100*i/len(groups)}%',end='\r')
        P=evaluateGroup(panels[panels.group==groupID])
        if P is not None:
            powers.append(P)
    return pd.DataFrame({'groupID':groups,'P':powers})

def productionByGroups(selectedSectionsFile,areasuitFile,powersFile,selectionTestFile):
    temp=pd.read_csv(powersFile)
    areasuit=gpd.read_file(areasuitFile)
    selectionTest=gpd.read_file(selectionTestFile)
    selectedSections=gpd.read_file(selectedSectionsFile)
    selectedSections['production']=np.nan
    production=[]
    for i,r in selectedSections.iterrows():
        print(f"{100*i/len(selectedSections)} %",end='\r')
        rids=areasuit[areasuit.SEZ2011==r.SEZ2011].FID.values
        ps=None
        for rid in rids:
            g=selectionTest[selectionTest.roofID==rid].group.unique()
            if len(g)!=0:
                for x in g:
                    p=np.array([float(v) for v in json.loads(temp[temp.groupID==x].P.values[0])])
                    if ps is None:
                        ps=p
                    else:
                        ps+=p
        if ps is None:
            ps=[0]
        production.append(sum(ps))
    selectedSections['production']=production
    selectedSections['kWh_pp']=(selectedSections.production/1000)/selectedSections.P1
    return selectedSections

