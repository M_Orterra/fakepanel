import geopandas as gpd
import numpy as np
import shapely
import math
from shapely.geometry import Point,Polygon


CONVERSTION_FACTOR=0.032/3600

def classicPlacement(roofRow,panelW=0.8,panelH=1.2):
    roof=roofRow["geometry"]
    panelW=panelW*CONVERSTION_FACTOR
    panelH=panelH*np.cos(np.deg2rad(roofRow['slope']))*CONVERSTION_FACTOR
    roofCenter=roof.centroid
    rotatedRoof=shapely.affinity.rotate(roof,roofRow['angle']-90,origin=roofCenter)
    _boundingBox=rotatedRoof.envelope
    axis_coords=list(_boundingBox.exterior.coords)
    axis1=Point(axis_coords[0]).distance(Point(axis_coords[1]))
    axis2=Point(axis_coords[1]).distance(Point(axis_coords[2]))
    numW=int(axis1/panelW)
    numH=int(axis2/panelH)
    nums=[numH+1,numW+1]
    startX=list(_boundingBox.exterior.coords)[0][0]
    startY=list(_boundingBox.exterior.coords)[0][1]
    panels=[]
    actualX=list(_boundingBox.exterior.coords)[0][0]
    actualY=list(_boundingBox.exterior.coords)[0][1]
    for i in range(nums[0]):
        for j in range(nums[1]):
            p=Polygon([(actualX,actualY),(actualX,actualY+panelH),(actualX+panelW,actualY+panelH),(actualX+panelW,actualY)])
            actualX+=panelW
            panels.append(p)
        actualY+=panelH
        actualX=startX
    _panels=[]
    for p in panels:
        _p=panel=shapely.affinity.rotate(p,90-roofRow['angle'],origin=roofCenter)
        _panels.append(_p)
    final_panels=[p for p in _panels if p.within(roof)==True]
    return final_panels

def getClassic(df):
    print("Placing panel with classic method")
    df=df.reset_index()
    #df=gpd.read_file(areaSuitSHP)
    classicRoofs=[]
    groups=[]
    roofIDs=[]
    c=1
    for i,elem in df.iterrows():
        panels=classicPlacement(elem)
        temp=panels[:-(len(panels)%8)]
        classicRoofs+=[x for x in temp]
        #check if the distance with the previous roof is shorter than 2 meters
        # we will keep the same group id
        if i>0 :
            if elem.geometry.distance(oldElem.geometry) > 2*CONVERSTION_FACTOR:
                c+=1
        groups+=[c for k in range(len(temp))]
        roofIDs+=[elem.FID for k in range(len(temp))]
        oldElem=elem
        print(f"{100*i/len(df):.3f}%",end="\r")
        
    return gpd.GeoDataFrame({"geometry":[p for p in classicRoofs],
                             "group":[x for x in groups],
                             "roofID":[x for x in roofIDs]}).set_geometry("geometry")
