#!/usr/bin/env python
# coding: utf-8

# In[20]:


import geopandas as gpd
import pandas as pd
import numpy as np
import random


def simulateSections(fileName):
    secToSim=pd.read_csv(fileName)
    r1=pd.read_csv("input/R01_indicatori_2011_sezioni.csv",sep=';')
    dataForSec=r1[r1["SEZ2011"].isin(secToSim.sections.values)]
    df=pd.read_csv('input/yearConsumption.csv')
    breaks=[0, 6257707.272222223, 13439563.54111111, 19344494.04888889, 27177505.89666667]
    df["cat"]=pd.cut(df.Wh,bins=breaks,labels=range(1,5))
    familySizes=['PF3','PF4','PF5','PF6']
    fakeHouses={}
    for i,r in dataForSec.iterrows():
        c=1
        print(f'{100*i/len(dataForSec)} %',end='\r')
        for size in familySizes:
            nOfFamilies=r[size]
            for x in range(nOfFamilies):
                building=random.choice(df[df.cat==c].buildingID.values)
                if fakeHouses.get(str(building),None):
                    fakeHouses[str(building)]+=1
                else:
                    fakeHouses[str(building)]=1
            c+=1
    temp=pd.read_csv('/media/HD/datasets/ned/yearly/270.csv')
    temp.t=pd.to_datetime(temp.t)
    temp=temp.set_index('t').resample('1h').mean()
    ned=pd.DataFrame({'t':temp.index,'P':[0.0 for x in temp.P.values]})
    for k,v in fakeHouses.items():
        temp=pd.read_csv('/media/HD/datasets/ned/yearly/'+k+'.csv')
        temp.t=pd.to_datetime(temp.t)
        temp=temp.set_index('t').resample('1h').mean()
        ned.P=ned.P.values+temp.P.values*v
    ned=ned.set_index('t')
    return ned

        




