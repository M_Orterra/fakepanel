import geopandas as gpd
from utils.classic import *

def selectSec(file,min_th,max_th):
    #  df=gpd.read_file('../input/torino_processed.shp')
    df=gpd.read_file(file)
    df=df.fillna(0)

    df=df[df.AreaVSDens<max_th]
    df=df[df.AreaVSDens >min_th]

    df=df.sort_values(by=["AreaVSDens"],ascending=False)
    #df.to_file('../output/selectedSec.shp')
    return df 

def selectSuitArea(file,sections):
    suit_area=gpd.read_file(file)
    selectedAreas=suit_area[suit_area.SEZ2011.isin(sections.SEZ2011.values)]
    #selectedAreas.to_file('../output/selectedAreas.shp')
    return selectedAreas

def selectPanels(areas):
    panels=getClassic(areas)
    return panels

