
import utils.classic as classic
import utils.selection as selection
import utils.production as production
import utils.houseSim as houseSim
if __name__ == "__main__":
	# print("Classic Test")
	# shp='input/areasuit1kW.shp'
	# output=classic.getClassic(shp)
	# output.to_file('output/classic.shp')
    
    # print ("selection test")
    # file='input/torino_processed.shp'
    # sections=selection.selectSec(file,0.16,3)
    # suitAreaFile='input/areasuit1kW.shp'
    # areas=selection.selectSuitArea(suitAreaFile,sections)
    # panels=selection.selectPanels(areas)
    # panels.to_file('output/selectionTest.shp')

    #production test
    # panelsFile='output/selectionTest.shp'
    # powers=production.evaluateAll(panelsFile)
    # powers.to_csv('output/powersTest.csv')

    # simulatedHouses=houseSim.simulateSections('output/secToSim.csv')
    # simulatedHouses.to_csv('output/simulatedHouses.csv')


    selectedSectionsFile='output/selectedSec.shp'
    areasuitFile='input/areasuit1kW.shp'
    powersFile='output/powersTest.csv'
    selectionTestFile='output/selectionTest.shp'
    final=production.productionByGroups(selectedSectionsFile,areasuitFile,powersFile,selectionTestFile)
    final.to_file('output/final.shp')
